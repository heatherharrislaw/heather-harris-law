At Heather Harris Law, every client counts. Our experienced criminal defense and family law attorneys care about you and your case and will fight for you! We have experienced attorneys and former prosecutors who will bring their knowledge of the courtroom to your case.

Address: 650 S Courtenay Pkwy, #101, Merritt Island, Florida 32952

Phone: 321-452-7055
